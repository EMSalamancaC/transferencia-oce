package bo.gob.aduana.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
@Scope("prototype")
public class EntrenadorBox implements Entrenador {

	@Value("${entrenador.email}")
	private String email;

	@Value("${entrenador.equipo}")
	private String equipo;

	private Consejo consejo;

	public EntrenadorBox() {
		//System.out.println("Construyendo instancia de entrenador de Box");
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}

	@Autowired
	public void setConsejo(@Qualifier("consejoFactory") Consejo consejo) {
		//System.out.println("Se inyecta la dependencia fortuna con el metodo setter");
		this.consejo = consejo;
	}

	public String getEmail() {
		return email;
	}

	public String getEquipo() {
		return equipo;
	}

	public String getRutina() {
		return "Sparring 5 rounds de 3 min por round";
	}

	public String getConsejo() {
		return "Box: " + consejo.getConsejo();
	}

	@PostConstruct
	public void initBean() {
		System.out.println("Inicia bean");
	}

	@PreDestroy
	public void destroyBean() {
		System.out.println("Destruye bean");
	}
}
