package bo.gob.aduana.java;

public interface Entrenador {

	String getRutina();

	String getConsejo();
}
