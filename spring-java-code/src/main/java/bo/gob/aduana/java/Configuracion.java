package bo.gob.aduana.java;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("bo.gob.aduana.java")
@PropertySource("classpath:capacitacion.properties")
public class Configuracion {
}
