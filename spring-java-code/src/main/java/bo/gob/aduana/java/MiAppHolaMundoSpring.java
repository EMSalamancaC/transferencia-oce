package bo.gob.aduana.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MiAppHolaMundoSpring {

	public static void main(String[] args) {

		//cargar el archivo de configuracio de Spring
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Configuracion.class);

		//Recuperar el bean del contenedor de spring
		Entrenador miEntrenador = context.getBean("entrenadorBasket", Entrenador.class);
		System.out.println(miEntrenador.getRutina());
		System.out.println(miEntrenador.getConsejo());

		//Recuperar el bean del contenedor de spring
		Entrenador miEntrenador2 = context.getBean("entrenadorFutbol", Entrenador.class);
		System.out.println(miEntrenador2.getRutina());
		System.out.println(miEntrenador2.getConsejo());

		//Crear entrenador de MMA
		EntrenadorBox entrenadorBox = context.getBean("entrenadorBox", EntrenadorBox.class);
		//entrenadorBox.setConsejo(ConsejoFactory.getConsejoServicio());
		System.out.println(entrenadorBox.getRutina());
		System.out.println(entrenadorBox.getConsejo());
		System.out.println(entrenadorBox.getEmail());
		System.out.println(entrenadorBox.getEquipo());

		//cerrar el contexto
		context.close();
	}
}
