package bo.gob.aduana.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MiAppBeansSpring {

	public static void main(String[] args) {

		//cargar el archivo de configuracio de Spring
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Configuracion.class);

		EntrenadorBox entrenador1 = context.getBean("entrenadorBox", EntrenadorBox.class);
		//entrenador1.setConsejo(ConsejoFactory.getConsejoServicio());
		System.out.println(entrenador1.getConsejo());

		EntrenadorBox entrenador2 = context.getBean("entrenadorBox", EntrenadorBox.class);
		//entrenador2.setConsejo(ConsejoFactory.getConsejoServicio());
		System.out.println(entrenador2.getConsejo());

		boolean iguales = entrenador1 == entrenador2;

		System.out.println("Son iguales? " + iguales);

		System.out.println(entrenador1);
		System.out.println(entrenador2);

		context.close();
	}
}
