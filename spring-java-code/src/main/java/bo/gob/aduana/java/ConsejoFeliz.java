package bo.gob.aduana.java;

import org.springframework.stereotype.Component;

@Component
public class ConsejoFeliz implements Consejo {

	public String getConsejo() {
		return "Hoy es tu dia de suerte";
	}
}
