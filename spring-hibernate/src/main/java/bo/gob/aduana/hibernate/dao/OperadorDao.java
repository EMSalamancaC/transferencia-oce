package bo.gob.aduana.hibernate.dao;

import bo.gob.aduana.hibernate.model.Operador;
import bo.gob.aduana.hibernate.model.OperadorEstado;

import java.util.List;

public interface OperadorDao {

	List<Operador> obtenerPorEstado(OperadorEstado estado);

	Operador obtenerPorDocumentoIdentificacion(String numero, String tipo);

	Operador obtenerPorId(Integer id);

	Operador guardar(Operador operador);
}
