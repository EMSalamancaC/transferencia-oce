package bo.gob.aduana.hibernate.repositories;

import bo.gob.aduana.hibernate.model.Aduana;
import bo.gob.aduana.hibernate.model.Operador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface AduanaRepositoryPostgres extends JpaRepository<Aduana, Integer> {

	Set<Aduana> getAduanaByOperadores(Operador operador);

	Set<Aduana> findAllByCodigoIn(Set<String> codigos);

	@Query("select a from Aduana a join a.operadores o where o.estado = 'HABILITADO'")
	List<Aduana> joinByOperador();
}
