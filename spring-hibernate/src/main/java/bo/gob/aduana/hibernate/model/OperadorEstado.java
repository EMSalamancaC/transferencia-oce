package bo.gob.aduana.hibernate.model;

public enum OperadorEstado {

	HABILITADO, SUSPENDIDO, DESHABILITADO, ELIMINADO
}
