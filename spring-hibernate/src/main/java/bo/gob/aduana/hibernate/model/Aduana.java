package bo.gob.aduana.hibernate.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "cap_aduana")
public class Aduana {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "descripcion")
	private String descripcion;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(
			name = "cap_operador_aduana",
			joinColumns = @JoinColumn(name = "aduana_id"),
			inverseJoinColumns = @JoinColumn(name = "operador_id")
	)
	@JsonIgnore
	private Set<Operador> operadores;

	public Aduana() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set<Operador> getOperadores() {
		return operadores;
	}

	public void setOperadores(Set<Operador> operadores) {
		this.operadores = operadores;
	}

	@Override
	public String toString() {
		return "Aduana{" +
				"id=" + id +
				", codigo='" + codigo + '\'' +
				", descripcion='" + descripcion + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Aduana aduana = (Aduana) o;
		return id.equals(aduana.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
