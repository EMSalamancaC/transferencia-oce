package bo.gob.aduana.hibernate.exception;

public class OceException extends Exception {

	public OceException(String message) {
		super(message);
	}
}
