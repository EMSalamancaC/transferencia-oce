package bo.gob.aduana.hibernate.repositories;

import bo.gob.aduana.hibernate.model.Medio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedioRepositoryPostgres extends JpaRepository<Medio, Integer> {
}
