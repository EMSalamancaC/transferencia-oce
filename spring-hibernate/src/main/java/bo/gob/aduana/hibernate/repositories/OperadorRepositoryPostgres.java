package bo.gob.aduana.hibernate.repositories;

import bo.gob.aduana.hibernate.model.Operador;
import bo.gob.aduana.hibernate.model.OperadorEstado;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OperadorRepositoryPostgres extends JpaRepository<Operador, Integer> {

	List<Operador> getAllByEstado(OperadorEstado estado);

	Operador getAllByDocumento_NumeroAndDocumento_Tipo(String numero, String tipo);
}
