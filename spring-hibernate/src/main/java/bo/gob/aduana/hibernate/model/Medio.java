package bo.gob.aduana.hibernate.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cap_medio")
public class Medio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "placa")
	private String placa;

	@Column(name = "marca")
	private String marca;

	@Column(name = "modelo")
	private String modelo;

	@ManyToOne
	@JoinColumn(name = "operador_id")
	@JsonBackReference
	private Operador operador;

	public Medio() {
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Operador getOperador() {
		return operador;
	}

	public void setOperador(Operador operador) {
		this.operador = operador;
	}

	@Override
	public String toString() {
		return "Medio{" +
				"id=" + id +
				", placa='" + placa + '\'' +
				", marca='" + marca + '\'' +
				", modelo='" + modelo + '\'' +
				", operador=" + operador.getId() +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Medio medio = (Medio) o;
		return id.equals(medio.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
