package bo.gob.aduana.hibernate.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cap_documento")
public class DocumentoIdentificacion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "tipo")
	private String tipo;

	@Column(name = "numero")
	private String numero;

	public DocumentoIdentificacion() {
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "DocumentoIdentificacion{" +
				"id='" + id + '\'' +
				", tipo='" + tipo + '\'' +
				", numero='" + numero + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		DocumentoIdentificacion that = (DocumentoIdentificacion) o;
		return id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
