package bo.gob.aduana.hibernate.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "cap_operador")
public class Operador {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "documento_id")
	private DocumentoIdentificacion documento;

	@Column(name = "razon_social")
	private String razonSocial;

	@Enumerated(EnumType.STRING)
	private OperadorTipo tipo;

	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "character varying (50) default 'HABILITADO'")
	private OperadorEstado estado = OperadorEstado.HABILITADO;

	@OneToMany(mappedBy = "operador", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, orphanRemoval = true)
	@JsonManagedReference
	private List<Medio> medios;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(
			name = "cap_operador_aduana",
			joinColumns = @JoinColumn(name = "operador_id"),
			inverseJoinColumns = @JoinColumn(name = "aduana_id")
	)
	@JsonIgnore
	private Set<Aduana> aduanas;

	public Operador() {
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public DocumentoIdentificacion getDocumento() {
		return documento;
	}

	public void setDocumento(DocumentoIdentificacion documento) {
		this.documento = documento;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public OperadorTipo getTipo() {
		return tipo;
	}

	public void setTipo(OperadorTipo tipo) {
		this.tipo = tipo;
	}

	public OperadorEstado getEstado() {
		return estado;
	}

	public void setEstado(OperadorEstado estado) {
		this.estado = estado;
	}

	public List<Medio> getMedios() {
		return medios;
	}

	public void setMedios(List<Medio> medios) {
		this.medios = medios;
	}

	public Set<Aduana> getAduanas() {
		return aduanas;
	}

	public void setAduanas(Set<Aduana> aduanas) {
		this.aduanas = aduanas;
	}

	@Override
	public String toString() {
		return "Operador{" +
				"id=" + id +
				", documento=" + documento +
				", razonSocial='" + razonSocial + '\'' +
				", tipo=" + tipo +
				", estado=" + estado +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Operador operador = (Operador) o;
		return id.equals(operador.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
