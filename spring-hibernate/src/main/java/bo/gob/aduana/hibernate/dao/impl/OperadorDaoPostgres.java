package bo.gob.aduana.hibernate.dao.impl;

import bo.gob.aduana.hibernate.dao.OperadorDao;
import bo.gob.aduana.hibernate.model.Operador;
import bo.gob.aduana.hibernate.model.OperadorEstado;
import bo.gob.aduana.hibernate.repositories.OperadorRepositoryPostgres;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperadorDaoPostgres implements OperadorDao {

	@Autowired
	OperadorRepositoryPostgres operadorRepository;

	@Override
	public List<Operador> obtenerPorEstado(OperadorEstado estado) {
		return operadorRepository.getAllByEstado(OperadorEstado.HABILITADO);
	}

	@Override
	public Operador obtenerPorDocumentoIdentificacion(String numero, String tipo) {
		return operadorRepository.getAllByDocumento_NumeroAndDocumento_Tipo(numero, tipo);
	}

	@Override
	public Operador obtenerPorId(Integer id) {
		return operadorRepository.findById(id).orElse(null);
	}

	@Override
	public Operador guardar(Operador operador) {
		return operadorRepository.save(operador);
	}
}
