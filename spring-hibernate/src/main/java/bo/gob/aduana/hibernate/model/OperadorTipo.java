package bo.gob.aduana.hibernate.model;

public enum OperadorTipo {

	IMPORTADOR, EXPORTADOR, TRANSPORTISTA, AGENCIA_DESPACHANTE
}
