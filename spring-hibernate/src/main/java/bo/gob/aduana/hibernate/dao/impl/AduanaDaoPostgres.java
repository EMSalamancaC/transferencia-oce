package bo.gob.aduana.hibernate.dao.impl;

import bo.gob.aduana.hibernate.dao.AduanaDao;
import bo.gob.aduana.hibernate.model.Aduana;
import bo.gob.aduana.hibernate.repositories.AduanaRepositoryPostgres;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AduanaDaoPostgres implements AduanaDao {

	@Autowired
	AduanaRepositoryPostgres aduanaRepositoryPostgres;

	public Set<Aduana> obtenerPorCodigo(Set<String> codigos) {
		return aduanaRepositoryPostgres.findAllByCodigoIn(codigos);
	}
}
