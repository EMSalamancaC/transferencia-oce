package bo.gob.aduana.hibernate.service.impl;

import bo.gob.aduana.hibernate.dao.AduanaDao;
import bo.gob.aduana.hibernate.dao.OperadorDao;
import bo.gob.aduana.hibernate.exception.OceException;
import bo.gob.aduana.hibernate.model.Aduana;
import bo.gob.aduana.hibernate.model.Operador;
import bo.gob.aduana.hibernate.model.OperadorEstado;
import bo.gob.aduana.hibernate.service.OperadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class OperadorServiceImpl implements OperadorService {

	@Autowired
	OperadorDao operadorDao;

	@Autowired
	AduanaDao aduanaDao;

	@Override
	public List<Operador> obtener() {
		return operadorDao.obtenerPorEstado(OperadorEstado.HABILITADO);
	}

	@Override
	public Operador crear(Operador operador) throws OceException {

		Operador existe = operadorDao.obtenerPorDocumentoIdentificacion(
				operador.getDocumento().getNumero(),
				operador.getDocumento().getTipo()
		);

		if (existe != null && !existe.getId().equals(operador.getId()))
			throw new OceException("Ya existe el operador");

		if (existe != null && (operador.getAduanas() == null || operador.getAduanas().size() == 0)) {

		}

		operador.setEstado(OperadorEstado.HABILITADO);
		return operadorDao.guardar(operador);
	}

	@Override
	public Operador asociarAduanas(Integer id, Set<String> aduanas) throws OceException {

		Operador operador = operadorDao.obtenerPorId(id);

		Set<Aduana> aduanasSet = aduanaDao.obtenerPorCodigo(aduanas);

		if (operador == null || aduanasSet == null || aduanasSet.size() == 0)
			throw new OceException("Datos no existen");

		operador.setAduanas(aduanasSet);
		return operadorDao.guardar(operador);
	}
}
