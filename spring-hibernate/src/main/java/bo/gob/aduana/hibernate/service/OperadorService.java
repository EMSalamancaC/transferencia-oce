package bo.gob.aduana.hibernate.service;

import bo.gob.aduana.hibernate.exception.OceException;
import bo.gob.aduana.hibernate.model.Operador;

import java.util.List;
import java.util.Set;

public interface OperadorService {

	List<Operador> obtener();

	Operador crear(Operador operador) throws OceException;

	Operador asociarAduanas(Integer id, Set<String> aduanas) throws OceException;
}
