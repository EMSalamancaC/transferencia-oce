package bo.gob.aduana.hibernate.dao;

import bo.gob.aduana.hibernate.model.Aduana;

import java.util.Set;

public interface AduanaDao {

	Set<Aduana> obtenerPorCodigo(Set<String> codigos);
}
