package bo.gob.aduana.hibernate.controller;

import bo.gob.aduana.hibernate.bean.Respuesta;
import bo.gob.aduana.hibernate.exception.OceException;
import bo.gob.aduana.hibernate.model.Operador;
import bo.gob.aduana.hibernate.service.OperadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(value = "/operador")
public class OperadorController {

	@Autowired
	OperadorService operadorService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public Respuesta obtener() {
		try {
			return new Respuesta(true, operadorService.obtener());
		} catch (Exception e) {
			e.printStackTrace();
			return new Respuesta(false, e.getMessage());
		}
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public Respuesta crear(@RequestBody Operador operador) {
		try {
			return new Respuesta(true, operadorService.crear(operador));
		} catch (OceException e) {
			e.printStackTrace();
			return new Respuesta(false, e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return new Respuesta(false, "500 " + e.getMessage());
		}
	}

	@RequestMapping(value = "/{id}/aduana", method = RequestMethod.POST)
	public Respuesta asociarAduanas(@PathVariable Integer id, @RequestBody Set<String> aduanas) {
		try {
			return new Respuesta(true, operadorService.asociarAduanas(id, aduanas));
		} catch (OceException e) {
			return new Respuesta(false, e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return new Respuesta(false, "500 " + e.getMessage());
		}
	}
}
