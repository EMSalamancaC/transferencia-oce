package bo.gob.aduana.hibernate.app;

import bo.gob.aduana.hibernate.config.ConfiguracionTest;
import bo.gob.aduana.hibernate.model.*;
import bo.gob.aduana.hibernate.repositories.AduanaRepositoryPostgres;
import bo.gob.aduana.hibernate.repositories.MedioRepositoryPostgres;
import bo.gob.aduana.hibernate.repositories.OperadorRepositoryPostgres;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ConfiguracionTest.class})
@WebAppConfiguration
public class MiAppTest {

	@Autowired
	OperadorRepositoryPostgres operadorRepository;

	@Autowired
	MedioRepositoryPostgres medioRepositoryPostgres;

	@Autowired
	AduanaRepositoryPostgres aduanaRepository;

	@Test
	public void start() {
		System.out.println("hola mundo hibernate");
	}

	@Test
	public void crearOperadorTest() {

		DocumentoIdentificacion documentoIdentificacion = new DocumentoIdentificacion();
		documentoIdentificacion.setNumero("1234567");
		documentoIdentificacion.setTipo("CI");

		Operador operador = new Operador();
		operador.setDocumento(documentoIdentificacion);
		operador.setRazonSocial("Marcelo Salamanca");
		operador.setTipo(OperadorTipo.EXPORTADOR);

		DocumentoIdentificacion documentoIdentificacion1 = new DocumentoIdentificacion();
		documentoIdentificacion1.setNumero("7654321");
		documentoIdentificacion1.setTipo("CI");

		Operador operador1 = new Operador();
		operador1.setDocumento(documentoIdentificacion1);
		operador1.setRazonSocial("Eliana James");
		operador1.setTipo(OperadorTipo.IMPORTADOR);

		DocumentoIdentificacion documentoIdentificacion2 = new DocumentoIdentificacion();
		documentoIdentificacion2.setNumero("111222333");
		documentoIdentificacion2.setTipo("NIT");

		Operador operador2 = new Operador();
		operador2.setDocumento(documentoIdentificacion2);
		operador2.setRazonSocial("Transportes International LTDA");
		operador2.setTipo(OperadorTipo.TRANSPORTISTA);

		DocumentoIdentificacion documentoIdentificacion3 = new DocumentoIdentificacion();
		documentoIdentificacion3.setNumero("333222111");
		documentoIdentificacion3.setTipo("NIT");

		Operador operador3 = new Operador();
		operador3.setDocumento(documentoIdentificacion3);
		operador3.setRazonSocial("Agencia Despachante de Aduana");
		operador3.setTipo(OperadorTipo.AGENCIA_DESPACHANTE);

		operadorRepository.save(operador);
		operadorRepository.save(operador1);
		operadorRepository.save(operador2);
		operadorRepository.save(operador3);
	}

	@Test
	public void crearAduanaTest() {

		Aduana aduana = new Aduana();
		aduana.setCodigo("201");
		aduana.setDescripcion("INTERIOR LA PAZ");

		Aduana aduana1 = new Aduana();
		aduana1.setCodigo("301");
		aduana1.setDescripcion("INTERIOR COCHABAMBA");

		Aduana aduana2 = new Aduana();
		aduana2.setCodigo("701");
		aduana2.setDescripcion("INTERIOR SANTA CRUZ");

		aduanaRepository.save(aduana);
		aduanaRepository.save(aduana1);
		aduanaRepository.save(aduana2);
	}

	@Test
	public void crearOperadoresMediosTest() {

		DocumentoIdentificacion documentoIdentificacion = new DocumentoIdentificacion();
		documentoIdentificacion.setNumero("1234567");
		documentoIdentificacion.setTipo("CI");

		Operador operador = new Operador();
		operador.setDocumento(documentoIdentificacion);
		operador.setRazonSocial("Marcelo Salamanca");
		operador.setTipo(OperadorTipo.EXPORTADOR);

		Medio medio = new Medio();
		medio.setPlaca("3431SRH");
		medio.setMarca("SUZUKI");
		medio.setModelo("SX4");
		medio.setOperador(operador);

		//medioRepositoryPostgres.save(medio);

		List<Medio> mediosList = new ArrayList<>();
		mediosList.add(medio);
		operador.setMedios(mediosList);

		operadorRepository.save(operador);
	}

	@Test
	public void crearOperadoresAduanasTest() {

		DocumentoIdentificacion documentoIdentificacion = new DocumentoIdentificacion();
		documentoIdentificacion.setNumero("1234567");
		documentoIdentificacion.setTipo("CI");

		Operador operador = new Operador();
		operador.setDocumento(documentoIdentificacion);
		operador.setRazonSocial("Marcelo Salamanca");
		operador.setTipo(OperadorTipo.EXPORTADOR);

		Aduana aduana = new Aduana();
		aduana.setCodigo("401");
		aduana.setDescripcion("INTERIOR ORURO");
		//aduanaRepository.save(aduana);

		Set<Aduana> aduanas = new HashSet<>();
		aduanas.add(aduana);
		operador.setAduanas(aduanas);

		operadorRepository.save(operador);
	}

	@Test
	public void agregarAduanaOperador() {

		Operador operador = operadorRepository.findById(2).orElse(null);
		Aduana aduana = aduanaRepository.findById(3).orElse(null);

		if (operador != null && aduana != null) {

			Set<Aduana> aduanas = aduanaRepository.getAduanaByOperadores(operador);
			aduanas.add(aduana);
			operador.setAduanas(aduanas);

			operadorRepository.save(operador);
		}
	}

	@Test
	public void eliminarOperadorAduana() {

		operadorRepository.deleteById(2);
	}

	@Test
	public void obtenerAduanasPorCodigo() {

		Set<String> codigos = new HashSet<>(Arrays.asList("201", "301", "701"));
		Set<Aduana> aduanasSet = aduanaRepository.findAllByCodigoIn(codigos);
		for (Aduana aduana : aduanasSet) {
			System.out.println(aduana);
		}
	}
}
