package bo.gob.aduana.xml;

import java.util.Random;

public class ConsejoFactory {

	public static Consejo getConsejoServicio() {

		System.out.println("Metodo Factory");

		int i = new Random().nextInt(3);

		switch (i) {
			case 0:
				return new ConsejoDisciplina();

			case 1:
				return new ConsejoSabiduria();

			case 2:
				return new ConsejoFeliz();

			default:
				return new ConsejoFeliz();
		}
	}
}
