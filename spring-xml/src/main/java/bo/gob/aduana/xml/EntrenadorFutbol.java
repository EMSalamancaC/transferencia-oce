package bo.gob.aduana.xml;

public class EntrenadorFutbol implements Entrenador {

	private Consejo consejo;

	public EntrenadorFutbol(Consejo consejo) {
		this.consejo = consejo;
	}

	public String getRutina() {
		return "Correr 2.4KM";
	}

	public String getConsejo() {
		return "Futbol: " + consejo.getConsejo();
	}
}
