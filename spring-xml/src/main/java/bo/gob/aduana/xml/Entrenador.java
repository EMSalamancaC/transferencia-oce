package bo.gob.aduana.xml;

public interface Entrenador {

	String getRutina();

	String getConsejo();
}
