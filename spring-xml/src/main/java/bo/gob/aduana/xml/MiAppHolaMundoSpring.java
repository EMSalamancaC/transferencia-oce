package bo.gob.aduana.xml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MiAppHolaMundoSpring {

	public static void main(String[] args) {

		//cargar el archivo de configuracio de Spring
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"file:/D:\\PROYECTOS\\ADUANA NACIONAL\\TRANSFERENCIA-OCE\\CODIGO-FUENTE\\spring-xml\\src\\main\\webapp\\WEB-INF\\applicationContext.xml"
		);

		//Recuperar el bean del contenedor de spring
		Entrenador miEntrenador = context.getBean("miEntrenador", Entrenador.class);

		//usar el bean
		System.out.println(miEntrenador.getRutina());

		//usar la dependencia de consejo
		System.out.println(miEntrenador.getConsejo());

		//Crear entrenador de MMA
		EntrenadorBox entrenadorBox = context.getBean("miEntrenadorBox", EntrenadorBox.class);
		System.out.println(entrenadorBox.getRutina());
		System.out.println(entrenadorBox.getConsejo());
		System.out.println(entrenadorBox.getEmail());
		System.out.println(entrenadorBox.getEquipo());

		//cerrar el contexto
		context.close();
	}
}
