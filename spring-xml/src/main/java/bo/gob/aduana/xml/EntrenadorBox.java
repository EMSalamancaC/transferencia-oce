package bo.gob.aduana.xml;

public class EntrenadorBox implements Entrenador {

	private String email;

	private String equipo;

	private Consejo consejo;

	public EntrenadorBox() {
		//System.out.println("Construyendo instancia de entrenador de Box");
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}

	public void setConsejo(Consejo consejo) {
		//System.out.println("Se inyecta la dependencia fortuna con el metodo setter");
		this.consejo = consejo;
	}

	public String getEmail() {
		return email;
	}

	public String getEquipo() {
		return equipo;
	}

	public String getRutina() {
		return "Sparring 5 rounds de 3 min por round";
	}

	public String getConsejo() {
		return "Box: " + consejo.getConsejo();
	}

	public void initBean() {
		System.out.println("Inicia bean");
	}

	public void destroyBean() {
		System.out.println("Destruye bean");
	}
}
