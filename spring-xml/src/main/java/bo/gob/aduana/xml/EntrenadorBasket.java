package bo.gob.aduana.xml;

public class EntrenadorBasket implements Entrenador {

	private Consejo consejo;

	public EntrenadorBasket(Consejo consejo) {
		this.consejo = consejo;
	}

	public String getRutina() {
		return "30 minutos de practica de lanzamiento";
	}

	public String getConsejo() {
		return "Basket: " + consejo.getConsejo();
	}
}
