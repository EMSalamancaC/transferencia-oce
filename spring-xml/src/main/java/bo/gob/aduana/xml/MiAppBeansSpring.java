package bo.gob.aduana.xml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MiAppBeansSpring {

	public static void main(String[] args) {

		//cargar el archivo de configuracio de Spring
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"file:/D:\\PROYECTOS\\ADUANA NACIONAL\\TRANSFERENCIA-OCE\\CODIGO-FUENTE\\spring-xml\\src\\main\\webapp\\WEB-INF\\beanContext.xml"
		);

		EntrenadorBox entrenador1 = context.getBean("miEntrenador1", EntrenadorBox.class);

		EntrenadorBox entrenador2 = context.getBean("miEntrenador1", EntrenadorBox.class);

		boolean iguales = entrenador1 == entrenador2;

		System.out.println("Son iguales? " + iguales);

		System.out.println(entrenador1);
		System.out.println(entrenador2);

		context.close();
	}
}
