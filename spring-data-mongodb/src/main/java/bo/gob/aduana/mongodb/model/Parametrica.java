package bo.gob.aduana.mongodb.model;

import java.util.Objects;

public class Parametrica {

	private String codigo;

	private String descripcion;

	public Parametrica() {
	}

	public Parametrica(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Parametrica{" +
				"codigo='" + codigo + '\'' +
				", descripcion='" + descripcion + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Parametrica that = (Parametrica) o;
		return codigo.equals(that.codigo);
	}

	@Override
	public int hashCode() {
		return Objects.hash(codigo);
	}
}
