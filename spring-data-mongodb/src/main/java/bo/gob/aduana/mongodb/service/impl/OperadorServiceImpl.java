package bo.gob.aduana.mongodb.service.impl;

import bo.gob.aduana.mongodb.dao.AduanaDao;
import bo.gob.aduana.mongodb.dao.OperadorDao;
import bo.gob.aduana.mongodb.exception.OceException;
import bo.gob.aduana.mongodb.model.Operador;
import bo.gob.aduana.mongodb.model.OperadorEstado;
import bo.gob.aduana.mongodb.service.OperadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperadorServiceImpl implements OperadorService {

	@Autowired
	OperadorDao operadorDao;

	@Autowired
	AduanaDao aduanaDao;

	@Override
	public List<Operador> obtenerTodos() {
		return operadorDao.obtenerTodos();
	}

	@Override
	public List<Operador> obtener() {
		return operadorDao.obtenerPorEstado(OperadorEstado.HABILITADO);
	}

	@Override
	public Operador obtenerPorId(String id) throws OceException {
		Operador operador = operadorDao.obtenerPorId(id);

		if (operador == null)
			throw new OceException("Operador no existe");

		return operador;
	}

	@Override
	public Operador crear(Operador operador) throws OceException {

		Operador existe = operadorDao.obtenerPorDocumentoIdentificacion(
				operador.getDocumento().getNumero(),
				operador.getDocumento().getTipo().getCodigo()
		);

		if (existe != null && !existe.getId().equals(operador.getId()))
			throw new OceException("Ya existe el operador");

		operador.setEstado(OperadorEstado.HABILITADO);
		return operadorDao.guardar(operador);
	}

	@Override
	public Operador deshabilitarPorId(String id) throws OceException {

		Operador operador = operadorDao.obtenerPorId(id);

		if (operador == null)
			throw new OceException("Operador no existe");

		if (operador.getEstado() == OperadorEstado.HABILITADO) {
			operador.setEstado(OperadorEstado.DESHABILITADO);
		} else {
			operador.setEstado(OperadorEstado.HABILITADO);
		}
		return operadorDao.guardar(operador);
	}
}
