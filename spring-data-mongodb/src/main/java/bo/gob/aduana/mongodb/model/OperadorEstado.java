package bo.gob.aduana.mongodb.model;

public enum OperadorEstado {

	HABILITADO, SUSPENDIDO, DESHABILITADO, ELIMINADO
}
