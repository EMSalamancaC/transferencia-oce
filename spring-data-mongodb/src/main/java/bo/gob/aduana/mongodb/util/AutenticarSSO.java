package bo.gob.aduana.mongodb.util;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class AutenticarSSO {

	private static String URL;
	private static final RestTemplate template = new RestTemplate();

	static {
		init();
	}

	public static Map<String, Object> verifySession(String token) {

		String url = URL + "autenticar/verificar/" + token;

		try {
			ResponseEntity<Map<String, Object>> responseEntity = template.exchange(
					url,
					HttpMethod.GET,
					null,
					new ParameterizedTypeReference<Map<String, Object>>() {
					}
			);
			return responseEntity.getBody();

		} catch (Exception e) {
			Map<String, Object> response = new HashMap<>();
			response.put("success", false);
			return response;
		}
	}

	public static void init() {
		try {
			InputStream inputStream = AutenticarSSO.class.getClassLoader().getResourceAsStream("urls.properties");
			Properties properties = new Properties();
			properties.load(inputStream);
			URL = properties.getProperty("application.sso");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
