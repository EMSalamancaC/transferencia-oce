package bo.gob.aduana.mongodb.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan("bo.gob.aduana.mongodb")
@PropertySource("classpath:capacitacion.properties")
public class Configuracion {
}
