package bo.gob.aduana.mongodb.repositories;

import bo.gob.aduana.mongodb.model.Operador;
import bo.gob.aduana.mongodb.model.OperadorEstado;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperadorRepositoryMongo extends CrudRepository<Operador, String> {

	List<Operador> getAllByEstado(OperadorEstado estado);

	Operador getAllByDocumento_NumeroAndDocumento_Tipo_Codigo(String numero, String codigo);
}
