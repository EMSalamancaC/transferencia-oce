package bo.gob.aduana.mongodb.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;

@Document
public class Operador {

	@Id
	private String id;

	private DocumentoIdentificacion documento;

	private String razonSocial;

	private Parametrica tipo;

	private OperadorEstado estado;

	private List<Medio> medios;

	@DBRef
	private Set<Aduana> aduanas;

	public Operador() {
	}

	public void agregarMedio(Medio medio) {
		if (medios == null) {
			medios = new ArrayList<>();
		}
		medios.add(medio);
	}

	public void agregarAduana(Aduana aduana) {
		if (aduanas == null) {
			aduanas = new HashSet<>();
		}
		aduanas.add(aduana);
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public DocumentoIdentificacion getDocumento() {
		return documento;
	}

	public void setDocumento(DocumentoIdentificacion documento) {
		this.documento = documento;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public Parametrica getTipo() {
		return tipo;
	}

	public void setTipo(Parametrica tipo) {
		this.tipo = tipo;
	}

	public OperadorEstado getEstado() {
		return estado;
	}

	public void setEstado(OperadorEstado estado) {
		this.estado = estado;
	}

	public List<Medio> getMedios() {
		return medios;
	}

	public void setMedios(List<Medio> medios) {
		this.medios = medios;
	}

	public Set<Aduana> getAduanas() {
		return aduanas;
	}

	public void setAduanas(Set<Aduana> aduanas) {
		this.aduanas = aduanas;
	}

	@Override
	public String toString() {
		return "Operador{" +
				"id=" + id +
				", documento=" + documento +
				", razonSocial='" + razonSocial + '\'' +
				", tipo=" + tipo +
				", estado=" + estado +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Operador operador = (Operador) o;
		return id.equals(operador.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
