package bo.gob.aduana.mongodb.dao;

import bo.gob.aduana.mongodb.model.Operador;
import bo.gob.aduana.mongodb.model.OperadorEstado;

import java.util.List;

public interface OperadorDao {

	List<Operador> obtenerTodos();

	List<Operador> obtenerPorEstado(OperadorEstado estado);

	Operador obtenerPorDocumentoIdentificacion(String numero, String codigo);

	Operador obtenerPorId(String id);

	Operador guardar(Operador operador);
}
