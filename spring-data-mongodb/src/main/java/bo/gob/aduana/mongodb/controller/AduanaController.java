package bo.gob.aduana.mongodb.controller;

import bo.gob.aduana.mongodb.bean.Respuesta;
import bo.gob.aduana.mongodb.service.AduanaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/aduana")
public class AduanaController {

	@Autowired
	AduanaService aduanaService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public Respuesta obtener() {
		try {
			return new Respuesta(true, aduanaService.obtener());
		} catch (Exception e) {
			e.printStackTrace();
			return new Respuesta(false, e.getMessage());
		}
	}
}
