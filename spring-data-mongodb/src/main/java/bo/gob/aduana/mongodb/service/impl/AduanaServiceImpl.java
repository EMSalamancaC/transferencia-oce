package bo.gob.aduana.mongodb.service.impl;

import bo.gob.aduana.mongodb.dao.AduanaDao;
import bo.gob.aduana.mongodb.model.Aduana;
import bo.gob.aduana.mongodb.service.AduanaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AduanaServiceImpl implements AduanaService {

	@Autowired
	AduanaDao aduanaDao;

	@Override
	public Set<Aduana> obtener() {
		return aduanaDao.obtenerTodas();
	}
}
