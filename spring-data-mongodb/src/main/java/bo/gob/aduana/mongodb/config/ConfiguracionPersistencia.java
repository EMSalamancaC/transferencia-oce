package bo.gob.aduana.mongodb.config;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@PropertySources({
		@PropertySource("classpath:/capacitacion.properties"),
		@PropertySource("file:${mongodb.property.file}")
})
@EnableMongoRepositories(basePackages = "bo.gob.aduana.mongodb.repositories", mongoTemplateRef = "primaryMongoTemplate")
public class ConfiguracionPersistencia extends AbstractMongoClientConfiguration {

	@Autowired
	private Environment environment;

	public @Bean(name = "primaryMongoTemplate")
	MongoTemplate getMongoTemplate() throws Exception {
		return new MongoTemplate(mongoClient(), getDatabaseName());
	}

	@Override
	public String getDatabaseName() {
		return environment.getProperty("mongodb.connection.database");
	}

	@Override
	public MongoClient mongoClient() {
		String connStr = "mongodb://"
				+ environment.getProperty("mongodb.connection.username")
				+ ":"
				+ environment.getProperty("mongodb.connection.password")
				+ "@"
				+ environment.getProperty("mongodb.connection.host")
				+ ":"
				+ environment.getProperty("mongodb.connection.port")
				+ "/?authSource="
				+ environment.getProperty("mongodb.connection.database");
		return MongoClients.create(connStr);
	}
}
