package bo.gob.aduana.mongodb.bean;

public class Respuesta {

	private Boolean success;

	private Object result;

	public Respuesta() {
	}

	public Respuesta(Boolean success, Object result) {
		this.success = success;
		this.result = result;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "Respuesta{" +
				"success=" + success +
				", result=" + result +
				'}';
	}
}
