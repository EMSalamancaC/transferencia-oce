package bo.gob.aduana.mongodb.dao.impl;

import bo.gob.aduana.mongodb.dao.OperadorDao;
import bo.gob.aduana.mongodb.model.Operador;
import bo.gob.aduana.mongodb.model.OperadorEstado;
import bo.gob.aduana.mongodb.repositories.OperadorRepositoryMongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperadorDaoMongo implements OperadorDao {

	@Autowired
	OperadorRepositoryMongo operadorRepository;

	public List<Operador> obtenerTodos() {
		return (List<Operador>) operadorRepository.findAll();
	}

	public List<Operador> obtenerPorEstado(OperadorEstado estado) {
		return operadorRepository.getAllByEstado(OperadorEstado.HABILITADO);
	}

	public Operador obtenerPorDocumentoIdentificacion(String numero, String codigo) {
		return operadorRepository.getAllByDocumento_NumeroAndDocumento_Tipo_Codigo(numero, codigo);
	}

	@Override
	public Operador obtenerPorId(String id) {
		return operadorRepository.findById(id).orElse(null);
	}

	@Override
	public Operador guardar(Operador operador) {
		return operadorRepository.save(operador);
	}
}
