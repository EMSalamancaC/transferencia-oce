package bo.gob.aduana.mongodb.controller;

import bo.gob.aduana.mongodb.bean.Respuesta;
import bo.gob.aduana.mongodb.exception.OceException;
import bo.gob.aduana.mongodb.model.Operador;
import bo.gob.aduana.mongodb.service.OperadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/operador")
public class OperadorController {

	@Autowired
	OperadorService operadorService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public Respuesta obtener() {
		try {
			//return new Respuesta(true, operadorService.obtener());
			return new Respuesta(true, operadorService.obtenerTodos());
		} catch (Exception e) {
			e.printStackTrace();
			return new Respuesta(false, e.getMessage());
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Respuesta obtenerPorId(@PathVariable String id) {
		try {
			return new Respuesta(true, operadorService.obtenerPorId(id));
		} catch (OceException e) {
			return new Respuesta(false, e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return new Respuesta(false, "500 " + e.getMessage());
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Respuesta deshabilitarPorId(@PathVariable String id) {
		try {
			return new Respuesta(true, operadorService.deshabilitarPorId(id));
		} catch (OceException e) {
			return new Respuesta(false, e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return new Respuesta(false, "500 " + e.getMessage());
		}
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public Respuesta crear(@RequestBody Operador operador) {
		try {
			return new Respuesta(true, operadorService.crear(operador));
		} catch (OceException e) {
			return new Respuesta(false, e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return new Respuesta(false, "500 " + e.getMessage());
		}
	}
}
