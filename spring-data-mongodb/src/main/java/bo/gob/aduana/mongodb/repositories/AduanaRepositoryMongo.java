package bo.gob.aduana.mongodb.repositories;

import bo.gob.aduana.mongodb.model.Aduana;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AduanaRepositoryMongo extends CrudRepository<Aduana, String> {

	Set<Aduana> findAll();

	Set<Aduana> findAllByCodigoIn(Set<String> codigos);
}
