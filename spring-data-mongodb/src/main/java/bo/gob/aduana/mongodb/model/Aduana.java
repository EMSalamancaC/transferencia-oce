package bo.gob.aduana.mongodb.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;
import java.util.Set;

@Document
public class Aduana {

	@Id
	private String id;

	private String codigo;

	private String descripcion;

	private Set<Operador> operadores;

	public Aduana() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set<Operador> getOperadores() {
		return operadores;
	}

	public void setOperadores(Set<Operador> operadores) {
		this.operadores = operadores;
	}

	@Override
	public String toString() {
		return "Aduana{" +
				"id=" + id +
				", codigo='" + codigo + '\'' +
				", descripcion='" + descripcion + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Aduana aduana = (Aduana) o;
		return id.equals(aduana.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
