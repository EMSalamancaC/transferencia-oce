package bo.gob.aduana.mongodb.service;

import bo.gob.aduana.mongodb.model.Aduana;

import java.util.Set;

public interface AduanaService {

	Set<Aduana> obtener();
}
