package bo.gob.aduana.mongodb.dao.impl;

import bo.gob.aduana.mongodb.dao.AduanaDao;
import bo.gob.aduana.mongodb.model.Aduana;
import bo.gob.aduana.mongodb.repositories.AduanaRepositoryMongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AduanaDaoMongo implements AduanaDao {

	@Autowired
	AduanaRepositoryMongo aduanaRepositoryMongo;

	@Override
	public Set<Aduana> obtenerTodas() {
		return aduanaRepositoryMongo.findAll();
	}

	public Set<Aduana> obtenerPorCodigo(Set<String> codigos) {
		return aduanaRepositoryMongo.findAllByCodigoIn(codigos);
	}
}
