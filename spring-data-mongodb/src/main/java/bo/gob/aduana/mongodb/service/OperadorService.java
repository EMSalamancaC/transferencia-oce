package bo.gob.aduana.mongodb.service;

import bo.gob.aduana.mongodb.exception.OceException;
import bo.gob.aduana.mongodb.model.Operador;

import java.util.List;

public interface OperadorService {

	List<Operador> obtenerTodos();

	List<Operador> obtener();

	Operador obtenerPorId(String id) throws OceException;

	Operador crear(Operador operador) throws OceException;

	Operador deshabilitarPorId(String id) throws OceException;
}
