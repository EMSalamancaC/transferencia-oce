package bo.gob.aduana.mongodb.model;

import java.util.Objects;

public class DocumentoIdentificacion {

	private Parametrica tipo;

	private String numero;

	public DocumentoIdentificacion() {
	}

	public Parametrica getTipo() {
		return tipo;
	}

	public void setTipo(Parametrica tipo) {
		this.tipo = tipo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "DocumentoIdentificacion{" +
				"tipo=" + tipo +
				", numero='" + numero + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		DocumentoIdentificacion that = (DocumentoIdentificacion) o;
		return numero.equals(that.numero);
	}

	@Override
	public int hashCode() {
		return Objects.hash(numero);
	}
}
