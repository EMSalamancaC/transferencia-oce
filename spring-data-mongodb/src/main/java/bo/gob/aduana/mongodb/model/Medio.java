package bo.gob.aduana.mongodb.model;

import java.util.Objects;

public class Medio {

	private String placa;

	private String marca;

	private String modelo;

	public Medio() {
	}

	public Medio(String placa) {
		this.placa = placa;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	@Override
	public String toString() {
		return "Medio{" +
				", placa='" + placa + '\'' +
				", marca='" + marca + '\'' +
				", modelo='" + modelo + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Medio medio = (Medio) o;
		return placa.equals(medio.placa);
	}

	@Override
	public int hashCode() {
		return Objects.hash(placa);
	}
}
