package bo.gob.aduana.mongodb.config;

import bo.gob.aduana.mongodb.util.AutenticarSSO;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class FiltroToken implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		Map<String, Object> jsonResult = null;

		if (request.getHeader("Auth-Token") != null) {
			jsonResult = AutenticarSSO.verifySession(request.getHeader("Auth-Token"));
		}

		if (jsonResult != null && Boolean.parseBoolean(String.valueOf(jsonResult.get("success")))) {
			filterChain.doFilter(request, response);
		} else {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "usuario no autenticado");
		}
	}

	@Override
	public void destroy() {

	}
}
