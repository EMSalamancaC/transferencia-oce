package bo.gob.aduana.mongodb.model;

public enum OperadorTipo {

	IMPORTADOR, EXPORTADOR, TRANSPORTISTA, AGENCIA_DESPACHANTE
}
