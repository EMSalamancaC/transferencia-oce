package bo.gob.aduana.mongodb.dao;

import bo.gob.aduana.mongodb.model.Aduana;

import java.util.Set;

public interface AduanaDao {

	Set<Aduana> obtenerPorCodigo(Set<String> codigos);

	Set<Aduana> obtenerTodas();
}
