package bo.gob.aduana.mongodb.exception;

public class OceException extends Exception {

	public OceException(String message) {
		super(message);
	}
}
