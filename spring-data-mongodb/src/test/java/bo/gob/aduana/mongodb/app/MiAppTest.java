package bo.gob.aduana.mongodb.app;

import bo.gob.aduana.mongodb.config.ConfiguracionTest;
import bo.gob.aduana.mongodb.model.*;
import bo.gob.aduana.mongodb.repositories.AduanaRepositoryMongo;
import bo.gob.aduana.mongodb.repositories.OperadorRepositoryMongo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ConfiguracionTest.class})
@WebAppConfiguration
public class MiAppTest {

	@Autowired
	@Qualifier(value = "primaryMongoTemplate")
	MongoTemplate mongoTemplate;

	@Autowired
	OperadorRepositoryMongo operadorRepository;

	@Autowired
	AduanaRepositoryMongo aduanaRepository;

	@Test
	public void start() {
		System.out.println("hola mundo spring-data-mongodb");
	}

	@Test
	public void guardarOperadorConComgoTemplate() {

		Operador operador = new Operador();
		//operador.setId(1);
		mongoTemplate.save(operador, "operadores");
	}

	@Test
	public void recuperarOperadores() {

		Iterable<Operador> operadores = operadorRepository.findAll();
		for (Operador operador : operadores) {
			System.out.println(operador);
		}
	}

	@Test
	public void guardarOperadorConRepository() {

		DocumentoIdentificacion identificacion = new DocumentoIdentificacion();
		identificacion.setTipo(new Parametrica("CI", "CÉDULA DE IDENTIDAD"));
		identificacion.setNumero("123123123");

		Operador operador = new Operador();
		operador.setDocumento(identificacion);
		operador.setRazonSocial("JUAN PEREZ");
		operador.setTipo(new Parametrica("IMP", "IMPORTADOR"));
		operador.setEstado(OperadorEstado.HABILITADO);

		Medio medio = new Medio();
		medio.setPlaca("1234ABC");
		medio.setMarca("JEEP");
		medio.setModelo("GRAND CHEROKEE");

		Medio medio2 = new Medio();
		medio2.setPlaca("3131CVB");
		medio2.setMarca("VOLKSWAGEN");
		medio2.setModelo("GOL");

		operador.agregarMedio(medio);
		operador.agregarMedio(medio2);

		Aduana aduana = new Aduana();
		aduana.setId("uno");
		aduana.setCodigo("201");
		aduana.setDescripcion("INTERIOR LA PAZ");

		Aduana aduana2 = new Aduana();
		aduana2.setId("dos");
		aduana2.setCodigo("301");
		aduana2.setDescripcion("INTERIOR COCHABAMBA");

		aduana = aduanaRepository.save(aduana);
		aduana2 = aduanaRepository.save(aduana2);

		operador.agregarAduana(aduana);
		operador.agregarAduana(aduana2);

		operadorRepository.save(operador);
	}

	@Test
	public void agregarMedioAOperador() {

		//Medio a agregar
		Medio medio = new Medio();
		medio.setPlaca("3131CVB");
		medio.setMarca("VOLKSWAGEN");
		medio.setModelo("GOL");

		Operador operador = operadorRepository.findById("5f4d34a9383c34285f917c29").orElse(null);
		if (operador != null) {
			List<Medio> medios = operador.getMedios();
			if (!medios.contains(medio)) {
				medios.add(medio);
				operadorRepository.save(operador);
			}
		}
	}

	@Test
	public void quitarMedioDeOperador() {

		//Medio a eliminar
		String placa = "1234ABC";

		Operador operador = operadorRepository.findById("5f4d34a9383c34285f917c29").orElse(null);
		if (operador != null) {
			List<Medio> medios = operador.getMedios();
			medios.remove(new Medio(placa));
			operadorRepository.save(operador);
		}
	}

	@Test
	public void agregarAduanaAOperador() {

		Aduana aduana = aduanaRepository.findById("5f4d4657d3a442025212f62c").orElse(null);
		Operador operador = operadorRepository.findById("5f4d34a9383c34285f917c29").orElse(null);
		System.out.println(operador);

		if (aduana != null && operador != null) {
			Set<Aduana> aduanas = operador.getAduanas();
			System.out.println(aduanas);

			aduanas.add(aduana);
			operadorRepository.save(operador);
		}
	}

	@Test
	public void quitarAduanaAOperador() {

		Aduana aduana = aduanaRepository.findById("5f4d4657d3a442025212f62c").orElse(null);
		Operador operador = operadorRepository.findById("5f4d34a9383c34285f917c29").orElse(null);

		if (aduana != null && operador != null) {
			Set<Aduana> aduanas = operador.getAduanas();
			aduanas.remove(aduana);
			operadorRepository.save(operador);
		}
	}

	@Test
	public void guardarAduana() {

		Aduana aduana = new Aduana();
		aduana.setCodigo("701");
		aduana.setDescripcion("INTERIOR SANTA CRUZ");

		aduanaRepository.save(aduana);
	}

	@Test
	public void obtenerAduanasPorCodigo() {

		Set<String> codigos = new HashSet<>(Arrays.asList("201", "301", "701"));
		Set<Aduana> aduanasSet = aduanaRepository.findAllByCodigoIn(codigos);
		for (Aduana aduana : aduanasSet) {
			System.out.println(aduana);
		}
	}
}
