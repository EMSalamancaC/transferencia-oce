package bo.gob.aduana.rest.controller;

import bo.gob.aduana.rest.model.Producto;
import bo.gob.aduana.rest.model.Respuesta;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/producto")
public class ProductoController {

	@RequestMapping(value = "/saludo/{nombre}", method = RequestMethod.GET)
	public String saludo(@PathVariable String nombre) {
		return "Hola Mundo " + nombre;
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	public Respuesta guardar(@RequestBody Producto producto) {
		System.out.println(producto);
		return new Respuesta(true, producto);
	}

	@RequestMapping(value = "/{cod}", method = RequestMethod.PUT)
	public Respuesta modificar(@RequestBody Producto producto, @PathVariable String cod) {
		System.out.println(cod + ". " + producto);
		return new Respuesta(true, "Producto " + cod + " modificado.");
	}

	@RequestMapping(value = "/{cod}", method = RequestMethod.GET)
	public Respuesta buscar(@PathVariable String cod) {
		System.out.println(cod);
		Producto producto = new Producto(cod, "Producto X", 50F, "SAMSUNG");
		return new Respuesta(true, producto);
	}

	@RequestMapping(value = "/{cod}", method = RequestMethod.DELETE)
	public Respuesta eliminar(@PathVariable String cod) {
		System.out.println(cod);
		return new Respuesta(true, "Producto " + cod + " eliminado.");
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public Respuesta request(
			@RequestParam Map<String, String> parametros,
			@RequestHeader("SUMA") String header
	) {
		System.out.println(parametros + ". " + header);
		return new Respuesta(true, parametros);
	}
}
