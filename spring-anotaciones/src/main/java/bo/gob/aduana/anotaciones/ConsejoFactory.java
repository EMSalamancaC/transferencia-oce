package bo.gob.aduana.anotaciones;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class ConsejoFactory {

	@Bean("consejoFactory")
	public static Consejo getConsejoServicio() {

		System.out.println("Metodo Factory");

		int i = new Random().nextInt(3);

		switch (i) {
			case 0:
				return new ConsejoDisciplina();

			case 1:
				return new ConsejoSabiduria();

			case 2:
				return new ConsejoFeliz();

			default:
				return new ConsejoFeliz();
		}
	}
}
