package bo.gob.aduana.anotaciones;

public interface Entrenador {

	String getRutina();

	String getConsejo();
}
