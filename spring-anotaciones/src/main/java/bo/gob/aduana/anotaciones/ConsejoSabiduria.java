package bo.gob.aduana.anotaciones;

import org.springframework.stereotype.Component;

@Component
public class ConsejoSabiduria implements Consejo {

	public String getConsejo() {
		return "Se rapido para escuchar y lento para hablar";
	}
}
