package bo.gob.aduana.anotaciones;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class EntrenadorBasket implements Entrenador {

	@Autowired
	@Qualifier("consejoSabiduria")
	private Consejo consejo;

	public EntrenadorBasket() {
	}

	public EntrenadorBasket(Consejo consejo) {
		this.consejo = consejo;
	}

	public String getRutina() {
		return "30 minutos de practica de lanzamiento";
	}

	public String getConsejo() {
		return "Basket: " + consejo.getConsejo();
	}
}
