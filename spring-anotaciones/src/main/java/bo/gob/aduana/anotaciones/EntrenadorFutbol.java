package bo.gob.aduana.anotaciones;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class EntrenadorFutbol implements Entrenador {

	private Consejo consejo;

	@Autowired
	public EntrenadorFutbol(@Qualifier("consejoFeliz") Consejo consejo) {
		this.consejo = consejo;
	}

	public String getRutina() {
		return "Correr 2.4KM";
	}

	public String getConsejo() {
		return "Futbol: " + consejo.getConsejo();
	}
}
