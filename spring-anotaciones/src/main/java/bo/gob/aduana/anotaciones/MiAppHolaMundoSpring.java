package bo.gob.aduana.anotaciones;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MiAppHolaMundoSpring {

	public static void main(String[] args) {

		//cargar el archivo de configuracio de Spring
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"file:/D:\\PROYECTOS\\ADUANA NACIONAL\\TRANSFERENCIA-OCE\\CODIGO-FUENTE\\spring-anotaciones\\src\\main\\webapp\\WEB-INF\\applicationContext.xml"
		);

		//Recuperar el bean del contenedor de spring
		Entrenador miEntrenador = context.getBean("entrenadorBasket", Entrenador.class);
		System.out.println(miEntrenador.getRutina());
		System.out.println(miEntrenador.getConsejo());

		//Recuperar el bean del contenedor de spring
		Entrenador miEntrenador2 = context.getBean("entrenadorFutbol", Entrenador.class);
		System.out.println(miEntrenador2.getRutina());
		System.out.println(miEntrenador2.getConsejo());

		//Crear entrenador de MMA
		EntrenadorBox entrenadorBox = context.getBean("entrenadorBox", EntrenadorBox.class);
		//entrenadorBox.setConsejo(ConsejoFactory.getConsejoServicio());
		System.out.println(entrenadorBox.getRutina());
		System.out.println(entrenadorBox.getConsejo());
		System.out.println(entrenadorBox.getEmail());
		System.out.println(entrenadorBox.getEquipo());

		//cerrar el contexto
		context.close();
	}
}
