app.service("ListaOCEService", function ($http) {

	const listaOCEService = {};
	const urlSUMA = "https://suma-dev.aduana.gob.bo/b-spring-mongodb/";

	listaOCEService.operadores = [];

	$http.get(urlSUMA + "operador")
		.success(function (data) {
			listaOCEService.operadores = data.result;
		})
		.error(function (data, status) {
			alert("Error!");
		});

	listaOCEService.recuperarOperador = function (id) {
		return $http.get(urlSUMA + "operador/" + id)
			.then(function (o) {
				if (o.data.success) {
					return o.data;
				} else {
					return null;
				}
			}, function (e) {
				return {"success": false, "result": e}
			});
	};

	listaOCEService.cambiarEstadoOperador = function (id) {
		return $http.delete(urlSUMA + "operador/" + id)
			.then(function (o) {
				return o.data;
			}, function (e) {
				return {"success": false, "result": e}
			});
	};

	listaOCEService.guardar = function (operador) {
		return $http.post(urlSUMA + "operador", operador)
			.then(function (o) {
				return o.data;
			}, function (e) {
				return {"success": false, "result": e}
			});
	};

	listaOCEService.tiposDocumentoParam = function () {
		return $http.get("https://suma-dev.aduana.gob.bo/b-param/rest/documentoIdentificacion")
			.then(function (o) {
				if (o.data.success) {
					return o.data.result;
				} else {
					return [];
				}
			});
	};

	listaOCEService.tiposOperadorParam = function () {
		return $http.get("https://suma-dev.aduana.gob.bo/b-param/rest/tipoOperador")
			.then(function (o) {
				if (o.data.success) {
					return o.data.result;
				} else {
					return [];
				}
			});
	}

	listaOCEService.aduanas = function () {
		return $http.get(urlSUMA + "aduana")
			.then(function (o) {
				if (o.data.success) {
					return o.data.result;
				} else {
					return [];
				}
			});
	}

	return listaOCEService;
});
