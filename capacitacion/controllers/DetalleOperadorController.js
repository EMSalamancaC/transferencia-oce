app.controller("DetalleOperadorController", ["$scope", "$routeParams", "$location", "$filter", "$timeout", "ListaOCEService",
	function ($scope, $routeParams, $location, $filter, $timeout, ListaOCEService) {

		$("#alertSuccess").hide();
		$("#alertError").hide();

		$scope.edicion = false;

		//$scope.formularioValido = true;
		if (!$routeParams.id) {
			//Nuevo Operador
			$scope.operador = {};
		} else {
			$scope.edicion = true;
			//Consultar Operador de backend
			ListaOCEService.recuperarOperador($routeParams.id).then(function (d) {
				if (d.success) {
					$scope.operador = d.result;
				} else {
					$("#alertError").show();
					$scope.alertError = d.result;
				}
			});
		}

		ListaOCEService.tiposDocumentoParam().then(function (d) {
			$scope.tiposDocumento = d;
		});

		ListaOCEService.tiposOperadorParam().then(function (d) {
			$scope.tiposOperador = d;
		});

		$scope.guardarOperador = function () {

			/*if (!$scope.frmOperadores.$valid) {
				$scope.formularioValido = false;
			} else {*/
			ListaOCEService.guardar($scope.operador).then(function (d) {
				if (d.success) {
					$("#alertSuccess").show();
					$timeout(function () {
						console.log(d);
						$location.path("/");
					}, 2000);

				} else {
					$("#alertError").show();
					$scope.alertError = d.result;
				}
			});

			/*}*/
		};

		$scope.guardarMedio = function (medio) {

			$("#modalMedios").modal("hide");

			if (medio != null) {
				$scope.operador.medios = $scope.operador.medios != null ? $scope.operador.medios : [];
				let aux = $filter('filter')($scope.operador.medios, {"placa": medio.placa}, true)[0];
				if (aux != null) {
					let index = $scope.operador.medios.indexOf(aux);
					$scope.operador.medios[index] = medio;
				} else {
					$scope.operador.medios.push(medio);
				}
				$scope.medio = {};
			}
		};

		$scope.clean = function () {
			$scope.medio = {};
		};

		$scope.editarMedio = function (placa) {
			$scope.medio = _.clone($filter('filter')($scope.operador.medios, {"placa": placa}, true)[0]);
			$("#modalMedios").modal("show");
		};

		$scope.eliminarMedio = function (placa) {
			let aux = $filter('filter')($scope.operador.medios, {"placa": placa}, true)[0];
			if (aux != null) {
				let index = $scope.operador.medios.indexOf(aux);
				$scope.operador.medios.splice(index, 1);
			}
		};
	}
]);
