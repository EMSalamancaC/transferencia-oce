app.controller("ListaOperadoresController", ["$scope", "$timeout", "$location", "ListaOCEService", function ($scope, $timeout, $location, ListaOCEService) {

	$("#alertSuccess").hide();
	$("#alertError").hide();

	$scope.operadores = ListaOCEService.operadores;

	$scope.cambiarEstado = function (operador) {
		ListaOCEService.cambiarEstadoOperador(operador.id).then(function (d) {
			if (d.success) {
				$("#alertSuccess").show();
				operador.estado = d.result.estado;
				$timeout(function () {
					console.log(d);
					$location.path("/");
					$("#alertSuccess").hide();
				}, 2000);

			} else {
				$("#alertError").show();
				$scope.alertError = d.result;
			}
		});
	};

	$scope.$watch(function () {
		return ListaOCEService.operadores;
	}, function (otro) {
		$scope.operadores = otro;
	})
}]);
