app.controller("AgregarAduanasController", ["$scope", "$routeParams", "$filter", "$timeout", "$location", "ListaOCEService",
	function ($scope, $routeParams, $filter, $timeout, $location, ListaOCEService) {

		$("#alertSuccess").hide();
		$("#alertError").hide();

		ListaOCEService.aduanas().then(function (d) {
			$scope.aduanas = d;
		});

		//Consultar Operador de backend
		ListaOCEService.recuperarOperador($routeParams.id).then(function (d) {
			if (d.success) {
				$scope.operador = d.result;
				$scope.operador.aduanas = $scope.operador.aduanas != null ? $scope.operador.aduanas : [];
				for (let i = 0; i < $scope.operador.aduanas.length; i++) {
					let aux = $filter('filter')($scope.aduanas, {"codigo": $scope.operador.aduanas[i].codigo}, true)[0];
					aux.value = true;
				}
			} else {
				$("#alertError").show();
				$scope.alertError = d.result;
			}
		});

		$scope.guardarAduanas = function () {

			$scope.operador.aduanas = [];

			for (let i = 0; i < $scope.aduanas.length; i++) {
				if ($scope.aduanas[i].value) {
					$scope.operador.aduanas.push({
						"id": $scope.aduanas[i].id,
						"codigo": $scope.aduanas[i].codigo,
						"descripcion": $scope.aduanas[i].descripcion
					});
				}
			}

			ListaOCEService.guardar($scope.operador).then(function (d) {
				if (d.success) {
					$("#alertSuccess").show();
					$timeout(function () {
						console.log(d);
						$location.path("/");
					}, 2000);

				} else {
					$("#alertError").show();
					$scope.alertError = d.result;
				}
			});
		}
	}])
