let app = angular.module('listaOCEApp', ["ngRoute", "ngCookies"]);

app.config(function ($routeProvider) {
	$routeProvider
		.when("/", {
			templateUrl: "views/listaOperadores.html",
			controller: "ListaOperadoresController"
		})
		.when("/operador", {
			templateUrl: "views/agregarOperador.html",
			controller: "DetalleOperadorController"
		})
		.when('/operador/editar/:id', {
			templateUrl: 'views/agregarOperador.html',
			controller: 'DetalleOperadorController'
		})
		.when('/operador/aduanas/:id', {
			templateUrl: 'views/agregarAduanas.html',
			controller: 'AgregarAduanasController'
		})
		.otherwise({
			redirectTo: "/"
		})
});

/*app.run(function ($rootScope, $http, $cookies) {
		$http.defaults.headers.common['Auth-Token'] = $cookies.token;
	}
);*/

app.factory('httpRequestInterceptor', function ($cookies) {
	return {
		request: function (config) {
			config.headers['Auth-Token'] = $cookies.token;
			return config;
		}
	};
});

app.config(function ($httpProvider) {
	$httpProvider.interceptors.push('httpRequestInterceptor');
});
